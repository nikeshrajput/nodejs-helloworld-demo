#! /bin/bash

helm lint .

helm inspect values . > /tmp/hello-world.values

#this command will deploy helm chart in dev environment for kubernetes cluster
helm install nodejs-helloworld-dev . --values /tmp/hello-world.values

#this command will deploy helm chart in prod environment for kubernetes cluster
helm install nodejs-helloworld-prod . --set hello.env="prod" --values /tmp/hello-world.values

#cheking for all pods availability 
kubectl get all