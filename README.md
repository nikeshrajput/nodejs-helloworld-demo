**Hello World nodejs application demo deployed using Kubernetes with Helm Charts**
===================================================================================

This project provides Dockerfile and Helm Charts for deploying a sample Node.js web application into any Kubernetes based environment.

**Prerequisites**
------------------
_Using this project assumes the following pre-requisites are met :_
--------------
    1. Docker Installed in server
    2. As we are deploying this application in Kubernetes ( Container Orchestration Tool ), You should be having a Kubernetes Cluster. 
       This can also be single node cluster running locally for example Minikube.
    3. Kubernetes Utility tool, kubectl installed and configured for your cluster.
    4. Helm Installed for deploying application in Kubernetes Cluster. 

**Dockerize Sample Node.js application**
-----------------------------------------
    1. nodejs-app folder in this project contains Dockerfile that dockerizes Hello World nodejs application.
    2. Docker commands to build and push docker image is :
        `docker build . -t <username>/<imagename>:<tagname>` #This will build docker image locally in your server
        `docker login -u <username> -p <password/token> <docker_registry_url>` #Providing credentials to login to docker registry where image need to be pushed
        `docker push <username>/<imagename>:<tagname>` #Dockerized image will be Published to Docker registry
    3. In case if you don't need to execute step 2, Docker image for the same can be found on https://hub.docker.com/repository/docker/nikeshrajput/nodejs-helloworld 

- Once Docker Image is built and published we can install helm charts and deploy application in Kubernetes Cluster

**Installing Helm Charts**
------------------------------
    1. helm-chart:nodejs-helloworld contains all the template yamls for deploying Hello World node.js app in Kubernetes.
    2. Modify values.yaml as per your requirements.
    3. Deployment script ( helm-script.sh ) file is also made available where you can run your deployment script for deploying application using helm.
        Below commands need to triggered from your end :
        - chmod +x helm-script.sh
        - ./helm-script.sh

- Once deployment script is triggered, now you will able to access application in web browser on http:<minikube_ipaddr>:<port_number>
    
- Note: minikube_ipaddr if you are running on Single Node Cluster and port number will be nodePort exposed to internet from service in Kubernetes.To check this you can try using command `minikube service <service_name> --url`
